#include <stdio.h>
#include <iostream>
#include <math.h>
#include <windows.h>
#include <utility>
#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <opencv2/nonfree/features2d.hpp>
#include <opencv2/imgproc/imgproc.hpp>

using namespace std;
using namespace cv;

bool isGoodBoundingbox(vector<Point2f>& bb)
{
    Point2f hg = bb[0];
    Point2f hd = bb[1];
    Point2f bd = bb[2];
    Point2f bg = bb[3];

    return (hg.x < hd.x && bg.x < bd.x
        && hg.y < bg.y && hd.y < bd.y);
}

Point findArea(std::vector<Point2f>& result,
			  std::vector<Point2f>& match, Mat& image)
{
    int subDivCol = 0;
    int colDiv = 0;
    int subDivRow = 0;
    int rowDiv = 0;

    if (image.cols < image.rows)
    {
        colDiv = 9;
        rowDiv = 12;
    }
	else
    {
        colDiv = 12;
        rowDiv = 9;
    }
    subDivCol = image.cols / colDiv;
    subDivRow = image.rows / rowDiv;

    vector<pair<int, Point2f>> bestMatchCol (colDiv * rowDiv, pair<int, Point2f>(0, Point2f()));

    for (int i=0; i < match.size(); ++i)
    {
        int x = floor(match[i].x / subDivCol);
        int y = floor(match[i].y / subDivRow);
        bestMatchCol[y * colDiv + x].first
            = bestMatchCol[y * colDiv + x].first + 1;
        bestMatchCol[y * colDiv + x].second = match[i];
    }
    int indice = 0;
    int max = 0;

    for (int i=0; i < bestMatchCol.size(); ++i)
    {
        if (bestMatchCol[i].first > max)
        {
            max = bestMatchCol[i].first;
            indice = i;
        }
    }
    int minMax = match.size() < 25 ? match.size() * 0.42 : 5; 
    if (max < minMax)
		return (Point(-1,-1));
    int x = indice % colDiv;
    int y = indice / colDiv;

    result[0] = Point2f(x * subDivCol, y * subDivRow);
    result[1] = Point2f((x + 1) * subDivCol, y * subDivRow);
    result[2] = Point2f((x + 1) * subDivCol, (y + 1) * subDivRow);
    result[3] = Point2f(x * subDivCol, (y + 1) * subDivRow);
    
    return bestMatchCol[indice].second;
}

string ExePath()
{
    char buffer[MAX_PATH];
    GetModuleFileName(NULL, buffer, MAX_PATH);
    string::size_type pos = string(buffer).find_last_of( "\\/" );
    return string(buffer).substr(0, pos);
}

int main(int argc, char** argv)
{
    int lo = 42;
	int up = 80;
	std::string entry;
	Mat img_object;
	Mat img_scene;
	std::string windowMatchesTitle = "Good Matches and Object detection";
	std::string windowMaskTitle = "Mask";
	bool refSet = false, imgSet = false;

	while (true)
	{
		if (!refSet)
		{
			//std::cout << "Current file: " << ExePath() << std::endl;
			//std::cout << "Enter ref image: ";
			//getline(cin, entry);
			//img_object = imread(entry, IMREAD_COLOR);
			img_object = imread("C:/ref.jpg", IMREAD_COLOR);
			refSet = true;
		}
		if (!imgSet)
		{
			std::cout << "Current file: " << ExePath() << std::endl;
			std::cout << "Enter image to compute: ";
			getline(cin, entry);
			//img_scene = imread("../../" + entry + ".jpg", IMREAD_COLOR);
			img_scene = imread("C:/img" + entry + ".jpg", IMREAD_COLOR);
			imgSet = true;
		}
		if (refSet && imgSet)
		{
			if(!img_object.data || !img_scene.data)
			{
				std::cout<<" --(!) Error reading images " <<std::endl;
				return -1; 
			}
			//Detect the keypoints using SURF Detector
			int minHessian = 600;

			SurfFeatureDetector detector(minHessian);

			std::vector<KeyPoint> keypoints_object, keypoints_scene;

			detector.detect(img_object, keypoints_object);
			detector.detect(img_scene, keypoints_scene);

			//Calculate descriptors
			SurfDescriptorExtractor extractor;
			Mat descriptors_object, descriptors_scene;

			extractor.compute(img_object, keypoints_object, descriptors_object);
			extractor.compute(img_scene, keypoints_scene, descriptors_scene);

			//Matching descriptor vectors using FLANN matcher
			FlannBasedMatcher matcher;
			std::vector<std::vector<DMatch>> matches;
			//Get good match only
			std::vector<DMatch> good_matches;
			//matcher.match(descriptors_object, descriptors_scene, matches);
			matcher.knnMatch(descriptors_object, descriptors_scene, matches, 2);

			//double max_dist = 0;
			//double min_dist = 100;
			//for(int i = 0; i < descriptors_object.rows; i++)
			//{
			//	double dist = matches[i].distance;
			//	if(dist < min_dist)
			//		min_dist = dist;
			//	if(dist > max_dist)
			//		max_dist = dist;
			//}
			//printf("-- Max dist : %f \n", max_dist);
			//printf("-- Min dist : %f \n", min_dist);
			//for(int i = 0; i < descriptors_object.rows; i++)
			//{
			//	if(matches[i].distance <3*min_dist)
			//		good_matches.push_back(matches[i]);
			//}
			for(int i = 0; i < min(descriptors_scene.rows-1,(int) matches.size()); i++) //THIS LOOP IS SENSITIVE TO SEGFAULTS
			{
				if((matches[i][0].distance < 0.7 * matches[i][1].distance)
					&& ((int) matches[i].size() <= 2 && (int) matches[i].size() > 0))
				{
					good_matches.push_back(matches[i][0]);
				}
			}
			Mat img_matches;
			Mat tmpMask, dstMask;
			bool hasACan = false;

			drawMatches(img_object, keypoints_object, img_scene, keypoints_scene,
				good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
				vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

			if (good_matches.size() > 4)
			{
				// Localize the object from img_1 in img_2
				std::vector<Point2f> obj;
				std::vector<Point2f> scene;

				for(size_t i = 0; i < good_matches.size(); i++)
				{
					//Get the keypoints from the good matches
					obj.push_back(keypoints_object[ good_matches[i].queryIdx ].pt);
					scene.push_back(keypoints_scene[ good_matches[i].trainIdx ].pt);
				}

				Mat H = findHomography(obj, scene, CV_RANSAC);

				//Get the corners from the image_1
				std::vector<Point2f> obj_corners(4);
				obj_corners[0] = Point(0,0);
				obj_corners[1] = Point(img_object.cols, 0);
				obj_corners[2] = Point(img_object.cols, img_object.rows);
				obj_corners[3] = Point(0, img_object.rows);
				std::vector<Point2f> scene_corners(4);

				perspectiveTransform(obj_corners, scene_corners, H);

				bool goodbb = isGoodBoundingbox(scene_corners);
				hasACan = true;
				if (!goodbb)
				{
					Point interestP = findArea(scene_corners, scene, img_matches);
					hasACan = interestP.x != -1;
				}

				//Mask
				int x = goodbb ? scene_corners[0].x : (scene_corners[0].x + scene_corners[1].x) /2;
				Point seed = Point(x,
					(scene_corners[0].y + scene_corners[3].y) / 2);

				Vec3b intensity = img_scene.at<Vec3b>(seed);

				if (intensity.val[0] > 125 && intensity.val[1] > 125)
					seed.x -= 12;

				int newMaskVal = 255;
				Scalar newVal = Scalar(120, 120, 120);

				int connectivity = 8;
				int flags = connectivity + (newMaskVal << 8) + FLOODFILL_FIXED_RANGE + FLOODFILL_MASK_ONLY;

				Mat mask2 = Mat::zeros(img_scene.rows + 2, img_scene.cols + 2, CV_8UC1);
				floodFill(img_scene, mask2, seed, newVal, 0, Scalar(lo, lo, lo), Scalar(up, up, up), flags);
				Mat mask = mask2(Range(1, mask2.rows - 1), Range(1, mask2.cols - 1));
				tmpMask = mask;
				dstMask = tmpMask;

				namedWindow(windowMaskTitle, CV_WINDOW_AUTOSIZE);
			
				if (hasACan)
				{
					imshow(windowMaskTitle, mask);
			
					//Draw green boundingbox
					Point2f offset((float)img_object.cols, 0);
					line(img_matches, scene_corners[0] + offset, scene_corners[1] + offset, Scalar(0, 255, 0), 4);
					line(img_matches, scene_corners[1] + offset, scene_corners[2] + offset, Scalar(0, 255, 0), 4);
					line(img_matches, scene_corners[2] + offset, scene_corners[3] + offset, Scalar(0, 255, 0), 4);
					line(img_matches, scene_corners[3] + offset, scene_corners[0] + offset, Scalar(0, 255, 0), 4);
				}
			}
			//Show detected matches
			Mat tmp, dst;
			tmp = img_matches;
			dst = tmp;
			namedWindow(windowMatchesTitle, CV_WINDOW_AUTOSIZE);
			imshow(windowMatchesTitle, dst);
			int c;
			while(true)
			{
				c = waitKey(10);

				if ((char)c == ' ') { break; }
				if ((char)c == 13)
				{
					imgSet = false;
					destroyWindow(windowMaskTitle);
					destroyWindow(windowMatchesTitle);
					break;
				}
				if ((char)c == 'u')
				{
					pyrUp(tmp, dst, Size(tmp.cols*2, tmp.rows*2));
					if (hasACan)
						pyrUp(tmpMask, dstMask, Size(tmpMask.cols*2, tmpMask.rows*2));
				}
				else if ((char)c == 'd')
				{
					pyrDown(tmp, dst, Size(tmp.cols/2, tmp.rows/2));
					if (hasACan)
						pyrDown(tmpMask, dstMask, Size(tmpMask.cols/2, tmpMask.rows/2));
				}
				if (hasACan)
				{
				   imshow(windowMaskTitle, dstMask);
				   tmpMask = dstMask;
				}
				imshow(windowMatchesTitle, dst);
				tmp = dst;
			}
			if ((char)c == ' ') { break; }
		}
	}

    return 0;
}